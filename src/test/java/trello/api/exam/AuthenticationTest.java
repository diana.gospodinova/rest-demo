package trello.api.exam;

import base.BaseTestSetup;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static com.telerikacademy.api.tests.Constants.*;
import static com.telerikacademy.api.tests.Endpoints.AUTH_ENDPOINT;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;

public class AuthenticationTest extends BaseTestSetup {

    @Test
    public void authenticationTest() {

        baseURI = format("%s%s", BASE_URL, AUTH_ENDPOINT);

        Response response = given()
                .queryParam("key", KEY)
                .queryParam("token", TOKEN)
                .when()
                .get();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        assertEquals(response.getBody().jsonPath().getString("fullName"), FULL_NAME,
                "Full names don't match.");

        System.out.println("Authentication works!");
    }
}
