package com.telerikacademy.api.tests;

public class Constants {

    public static final String KEY = "f4e4284e53e402dcc8f27643a157c78a";
    public static final String TOKEN = "62241ace8e793eb232fed8d6a176fde689a1b7ede5c34aac61784c7cd2ac8e71";

    public static final String BASE_URL = "https://api.trello.com";

    public static final String FULL_NAME = "Diana Gospodinova";
    public static final String BOARD_NAME = "Web Services Exam";
    public static String BOARD_ID;
    public static final String NEW_LIST_NAME = "In Test";
    public static String IN_TEST_LIST_ID;
    public static String TO_DO_LIST_ID;
    public static String CARD_ID;
    public static final String CARD_NAME = "My first card";
    public static final String CARD_DESCRIPTION = "This card shows update options for a card via Trello API";

}
